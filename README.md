# README 4 WANDERIO#

### Cose che avrei fatto in produzione ma evitate per i fini del test ###

* L'app non andava nella root della repository, ma per mostrarvela è stato necessario farlo (vedi: [gordanblake.bitbucket.org](http://gordanblake.bitbucket.org)).
* Le librerie (js, css, ecc) sarebbero state gestite da Bower, non a mano.
* Avrei configurato Grunt o Gulp per il deploy, la minificazione e concatenazione di tutti quei files che ora sono singoli, come le direttive ad esempio.
* Ho voluto provare [purecss.io](http://purecss.io) al posto di bootstrap, è molto interessante e leggero, ma a quanto pare le media query fanno i capricci su Firefox, il responsive è testato solo su Chrome.


Saluti, *Giordano*.