app.directive('offerItinerary', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            offer: '='
        },
        templateUrl: 'js/directives/itinerary.html'
    };
});
