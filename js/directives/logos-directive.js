app.directive('offerLogo', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            offer: '='
        },
        templateUrl: 'js/directives/logos.html'
    };
});
