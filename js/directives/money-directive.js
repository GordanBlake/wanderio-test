app.directive('money', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            price: '='
        },
        template: '<span class="price"><small class="price-symbol" ng-if="price.amount">{{price.symbol}}</small><span>{{price ? price.amount.split(".")[0] : "-"}}</span><small ng-if="price.amount">{{amt[1] || ".00"}}</small></span>'
    };
});
