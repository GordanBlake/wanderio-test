// the offer results directive ////////////////////////////////////////////////////////////////////

app.directive('offerElement', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            offer: '='
        },
        templateUrl: 'js/directives/offer.html',
        controller: 'MainCtrl'
    };
});
