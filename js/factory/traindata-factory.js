// http call to get the data / using a factory to make it reusable elsewere ///////////////////

app.factory('trainData', ['$http', function($http) {
    return $http.get('data/products.json')
        .success(function(data) {
            return data;
        })
        .error(function(err) {
            return err;
        });
}]);
