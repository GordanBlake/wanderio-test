'use strict';


var app = angular.module('wanderioTest', [])


    .controller('MainCtrl', ['$scope', 'trainData', function($scope, trainData) {


        // entry point ////////////////////////////////////////////////////////////////////////////
        trainData.success(function(results) {
            $scope.offers = results.data.products;
        });




        // automatically finds and selects che cheapest offer /////////////////////////////////////
        $scope.selectCheapest = function($index) {
            var cheapestPrice = Infinity;
            var cheapestClassIndex;
            var cheapestFareIndex;

            var offer = $scope.offers[$index].classes;

            for(var i = offer.length-1; i >= 0 ; i--) {
                for(var j = offer[i].fares.length-1; j >= 0; j--) {
                    if (offer[i].fares[j].price && +offer[i].fares[j].price.amount < cheapestPrice) {
                        cheapestPrice = +offer[i].fares[j].price.amount;
                        cheapestClassIndex = i;
                        cheapestFareIndex = j;
                    }
                }
            }

            $scope.setSelectedOffer($index, cheapestClassIndex, cheapestFareIndex);
        };


        // the user willingly clicks a price from the list ////////////////////////////////////////
        $scope.userSelectFare = function(offer, selClass, selFare) {
            $scope.setSelectedOffer(offer, selClass, selFare);
        };

        // adds the right indexes to the selected offer field /////////////////////////////////////
        $scope.setSelectedOffer = function(index, clas, fare) {
            $scope.offers[index].selectedOffer = {
                class: clas,
                fare: fare
            };
        };

        // view: toggle the details from the offer list ///////////////////////////////////////////
        $scope.expandOffer = function($event) {
            var $currTarg = $($event.currentTarget);

            $('.offer').removeClass('selected');
            $('#cart').addClass('hidden');
            $('.select-button').prop('disabled', false).removeClass('confirmed');

            $currTarg.prop('disabled', true);
            $currTarg.closest('.offer').toggleClass('selected');
        };

        // adds the offer to the cart /////////////////////////////////////////////////////////////
        $scope.addToCart = function(index, $event) {
            $scope.cartOffer = index;
            $('#cart').removeClass('hidden');
            $($event.currentTarget).closest('.offer').removeClass('selected').find('.select-button').addClass('confirmed');
        };


    }]);